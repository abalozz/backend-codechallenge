<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class TimeInterval implements Rule
{
    protected $end_time;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($end_time)
    {
        $this->end_time = $end_time;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $start_time = date_create_immutable($value);
        $end_time = date_create_immutable($this->end_time);

        if (!$start_time || !$end_time) {
            return false;
        }

        $diff = $start_time->diff($end_time);

        if ($diff->invert) {
            return false;
        }

        return $diff->h >= 1 && $diff->h <= 8;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Delivery must be in an interval between 1 - 8 hours';
    }
}

<?php

namespace App\Http\Controllers;

use App\Order;
use App\Rules\TimeInterval;
use Illuminate\Http\Request;
use App\Jobs\AssignDriverToOrder;
use Illuminate\Support\Facades\Cache;

class OrderController extends Controller
{
    public function index(Request $request)
    {
        $cache_key = "orders-{$request->get('driver')}-{$request->get('date')}";

        $orders = Cache::get($cache_key, function () use ($request) {
            $query = Order::query();
            $query->where('delivery_date', $request->get('date'));
            $query->where('driver_id', $request->get('driver'));
            $query->with('customer');

            return $query->get()->map(function ($order) {
                return [
                    'name' => $order->customer->name,
                    'email' => $order->customer->email,
                    'telephone' => $order->telephone,
                    'delivery_address' => $order->delivery_address,
                    'delivery_date' => $order->delivery_date,
                    'start_delivery_time' => $order->start_delivery_time,
                    'end_delivery_time' => $order->end_delivery_time,
                ];
            });
        });

        return ['orders' => $orders];
    }

    public function create(Request $request)
    {
        $request->validate([
            'telephone' => 'required',
            'delivery_address' => 'required',
            'delivery_date' => 'required|date_format:Y-m-d',
            'start_delivery_time' => ['required', 'date_format:H:i', new TimeInterval($request->get('end_delivery_time'))],
            'end_delivery_time' => 'required|date_format:H:i',
            'customer_id' => 'required',
        ]);

        $order = new Order($request->only([
            'telephone',
            'delivery_address',
            'delivery_date',
            'start_delivery_time',
            'end_delivery_time',
            'customer_id',
        ]));

        $order->save();

        AssignDriverToOrder::dispatch($order);
    }
}

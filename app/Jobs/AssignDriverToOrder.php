<?php

namespace App\Jobs;

use App\Order;
use App\Driver;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Filesystem\Cache;
use Illuminate\Cache\CacheManager;

class AssignDriverToOrder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $order;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(CacheManager $cache)
    {
        $driver = Driver::inRandomOrder()->first();
        $this->order->driver()->associate($driver);
        $this->order->save();
        $cache->forget("orders-{$this->order->driver_id}-{$this->order->delivery_date}");
    }
}

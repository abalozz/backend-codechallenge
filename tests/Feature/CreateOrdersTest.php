<?php

namespace Tests\Feature;

use App\Customer;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateOrdersTest extends TestCase
{
    use RefreshDatabase;

    function test_customer_can_create_an_order()
    {
        $customer = factory(Customer::class)->create();

        $response = $this->json('POST', '/orders', [
            'telephone' => '612345678',
            'delivery_address' => 'Dirección de entrega N42',
            'delivery_date' => '2018-08-31',
            'start_delivery_time' => '20:00',
            'end_delivery_time' => '22:00',
            'customer_id' => $customer->id,
        ]);

        $response->assertOk();

        $this->assertDatabaseHas('orders', [
            'telephone' => '612345678',
            'delivery_address' => 'Dirección de entrega N42',
            'delivery_date' => '2018-08-31',
            'start_delivery_time' => '20:00',
            'end_delivery_time' => '22:00',
            'customer_id' => $customer->id,
        ]);
    }

    function test_send_data_in_an_invalid_format_returns_a_validation_error()
    {
        $response = $this->json('POST', '/orders', [
            'telephone' => '',
            'delivery_address' => '',
            'delivery_date' => '312-2018-08-31',
            'start_delivery_time' => 'not a valid time',
            'end_delivery_time' => '---',
        ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors([
            'telephone',
            'delivery_address',
            'delivery_date',
            'start_delivery_time',
            'end_delivery_time',
            'customer_id',
        ]);
    }

    function test_invalid_delivery_time_returns_a_validation_error()
    {
        $customer = factory(Customer::class)->create();

        $response = $this->json('POST', '/orders', [
            'telephone' => '612345678',
            'delivery_address' => 'Dirección de entrega N42',
            'delivery_date' => '2018-08-31',
            'start_delivery_time' => '10:00',
            'end_delivery_time' => '22:00',
            'customer_id' => $customer->id,
        ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors('start_delivery_time');
        $response->assertJson([
            'errors' => [
                'start_delivery_time' => [
                    'Delivery must be in an interval between 1 - 8 hours',
                ]
            ]
        ]);

        $this->assertDatabaseMissing('orders', [
            'telephone' => '612345678',
            'delivery_address' => 'Dirección de entrega N42',
            'delivery_date' => '2018-08-31',
            'start_delivery_time' => '10:00',
            'end_delivery_time' => '22:00',
            'customer_id' => $customer->id,
        ]);
    }
}

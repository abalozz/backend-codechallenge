<?php

namespace Tests\Feature;

use App\Order;
use App\Driver;
use App\Customer;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GetOrderTest extends TestCase
{
    use RefreshDatabase;

    function test_driver_can_get_orders()
    {
        $customer = factory(Customer::class)->create([
            'name' => 'Customer name',
            'email' => 'fake@email.com',
        ]);

        $driver = factory(Driver::class)->create();

        factory(Order::class)->create([
            'telephone' => '612345678',
            'delivery_address' => 'Dirección de entrega N42',
            'delivery_date' => '2018-08-31',
            'start_delivery_time' => '20:00',
            'end_delivery_time' => '22:00',
            'customer_id' => $customer->id,
            'driver_id' => $driver->id,
        ]);

        $response = $this->get("/orders?driver={$driver->id}&date=2018-08-31");

        $response->assertOk();
        $response->assertJson([
            'orders' => [
                [
                    'name' => 'Customer name',
                    'email' => 'fake@email.com',
                    'telephone' => '612345678',
                    'delivery_address' => 'Dirección de entrega N42',
                    'delivery_date' => '2018-08-31',
                    'start_delivery_time' => '20:00',
                    'end_delivery_time' => '22:00',
                ],
            ],
        ]);
    }

    function test_driver_can_get_more_than_1_order()
    {
        $driver = factory(Driver::class)->create();

        factory(Order::class)->times(3)->create([
            'delivery_date' => '2018-08-31',
            'driver_id' => $driver->id,
        ]);

        $response = $this->get("/orders?driver={$driver->id}&date=2018-08-31");

        $response->assertOk();
        $response->assertJsonCount(3, 'orders');
    }

    function test_driver_only_get_their_orders()
    {
        $driver = factory(Driver::class)->create();
        $driver2 = factory(Driver::class)->create();

        factory(Order::class)->create([
            'delivery_date' => '2018-08-31',
            'delivery_address' => 'Address for driver 1',
            'driver_id' => $driver->id,
        ]);

        factory(Order::class)->create([
            'delivery_date' => '2018-08-31',
            'delivery_address' => 'Address for driver 2',
            'driver_id' => $driver2->id,
        ]);

        $response = $this->get("/orders?driver={$driver->id}&date=2018-08-31");

        $response->assertOk();
        $response->assertJsonCount(1, 'orders');
        $response->assertJson([
            'orders' => [['delivery_address' => 'Address for driver 1']],
        ]);
        $response->assertJsonMissing([
            'orders' => [['delivery_address' => 'Address for driver 2']],
        ]);
    }

    function test_driver_do_not_get_orders_of_a_not_requested_date()
    {
        $driver = factory(Driver::class)->create();

        factory(Order::class)->create([
            'delivery_date' => '2018-08-31',
            'driver_id' => $driver->id,
        ]);

        factory(Order::class)->create([
            'delivery_date' => '2018-08-30',
            'driver_id' => $driver->id,
        ]);

        $response = $this->get("/orders?driver={$driver->id}&date=2018-08-31");

        $response->assertOk();
        $response->assertJsonCount(1, 'orders');
        $response->assertJson([
            'orders' => [['delivery_date' => '2018-08-31']],
        ]);
        $response->assertJsonMissing([
            'orders' => [['delivery_date' => '2018-08-30']],
        ]);
    }
}
